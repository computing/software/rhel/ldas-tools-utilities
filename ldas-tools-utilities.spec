# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define cmakehelper_ver 1.2.3
%define ldasgen_ver     2.7.3
%define diskcache_ver   2.7.2
%define frameutils_ver  2.6.6
%define _docdir %{_datadir}/doc/%{name}-%{version}

%define check_boost169 ( 0%{?rhel} && 0%{?rhel} <= 8 )
%define check_cmake3  ( 0%{?rhel} && 0%{?rhel} <= 7 )
%define check_python2 ( 0%{?rhel} && 0%{?rhel} <= 7 )
%define check_python3 ( 0%{?__python3:1} || ( 0%{?rhel} >= 7 ) )

Summary: LDAS tools utility programs
Name: ldas-tools-utilities
Version: 2.6.7
Release: 1.1%{?dist}
License: GPLv2+
Summary: LDAS Tools utilities derived from multiple LDAS Tools libraries
URL: "https://wiki.ligo.org/Computing/LDASTools"
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: https://software.igwn.org/lscsoft/source/ldas-tools-utilities-%{version}.tar.gz
%if %check_boost169
Requires: boost169-filesystem
Requires: boost169-program-options
Requires: boost169-regex
%else
Requires: boost-filesystem
Requires: boost-program-options
Requires: boost-regex
%endif
Requires: ldas-tools-diskcacheAPI >= %{diskcache_ver}
Requires: ldas-tools-frameAPI >= %{frameutils_ver}
%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
Buildrequires: ldas-tools-cmake >= %{cmakehelper_ver}
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: make
BuildRequires: rpm-build
Buildrequires: pkgconfig
Buildrequires: doxygen, graphviz
%if %check_boost169
BuildRequires: boost169-devel
%else
BuildRequires: boost-devel
%endif
Buildrequires: ldas-tools-ldasgen-devel >= %{ldasgen_ver}
Buildrequires: ldas-tools-diskcacheAPI-devel >= %{diskcache_ver}
Buildrequires: ldas-tools-frameAPI-devel >= %{frameutils_ver}

%description



%package doc
Group: Development/Scientific
Summary: LDAS tools utilities documentation
%description doc
This provides the documentation for the LDAS Tools utilities

#========================================================================

%prep
%setup -c -T -D -a 0 -n %{name}-%{version}

%build
%if %{check_boost169}
export BOOST_CONFIGURE_OPTIONS="-DBoost_NO_SYSTEM_PATHS=True -DBOOST_INCLUDEDIR=%{_includedir}/boost169/ -DBOOST_LIBRARYDIR=%{_libdir}/boost169/"
%else
export BOOST_CONFIGURE_OPTIONS="-DBOOST_INCLUDEDIR=%{_includedir}/boost/ -DBOOST_LIBRARYDIR=%{_libdir}"
%endif
%if %{check_cmake3}
export CMAKE_PROGRAM=cmake3
%else
export CMAKE_PROGRAM=cmake
%endif

${CMAKE_PROGRAM} \
    ${BOOST_CONFIGURE_OPTIONS} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    %{name}-%{version}

%install
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_docdir}/README

%check
%if %{check_cmake3}
export CTEST_PROGRAM=ctest3
%else
export CTEST_PROGRAM=ctest
%endif
${CTEST_PROGRAM} -V %{?_smp_mflags}

%clean
rm -rf %{buildroot}

%files
%{_bindir}/*

%files doc
%defattr(-,root,root)
%{_docdir}

%changelog
* Wed Sep 29 2021 Edward Maros <ed.maros@ligo.org> - 2.6.7-1
- Built for new release

* Tue Mar 9 2021 Edward Maros <ed.maros@ligo.org> - 2.6.6-1
- Built for new release

* Wed Aug 14 2019 Edward Maros <ed.maros@ligo.org> - 2.6.5-1
- Built for new release

* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
